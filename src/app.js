'use strict';

define([
        'resources/ivds',
        'resources/nodes',
        'text!config/settings.json',
        'text!resources/skins.json',
        'resources/skins',
        'js/overlays',
        'text!config/playerOptions.json'
    ],
    function (ivds, nodes, settings, skinsJSON, skinsJS, overlays, playerOptions) {

        // app.js variables
        // hold a reference to the Interlude Player
        var player;
        var head = 'time_detectives.Intro';
        settings = JSON.parse(settings);

        function onNodeStart() {
            console.log('node has started');
        }

        // project essentials
        return {
            settings: settings,

            // The onPlayerInit hook runs after the player was loaded.
            // Here you can load all the resources needed for playing the project
            // This function should end by adding a node to the playlist
            // If you're hook is doing anything asynchronous you need to return a promise. You can use the when.js libaray
            // that is exposed on the ctx object (e.g. return ctx.when.promise())
            onPlayerInit: function (ctx) { // jshint ignore:line
                player = ctx.player;

                // nodes
                player.repository.add(nodes);

                // decisions
                for (var i = 0; i < nodes.length; i++) {
                    if (nodes[i].data && nodes[i].data.decision && nodes[i].data.decision.children && nodes[i].data.decision.children.length > 0) {
                        player.decision.add(nodes[i].id);
                    }
                }

                // Graph
                player.graph.setHead(head);

                // GUI
                player.gui.addSkin({
                    id: 'defaultIndication',
                    url: 'https://d1w2zhnqcy4l8f.cloudfront.net/treehouseResources/parallel_indications/defaultParallelIndication.iskn'
                });
                player.gui.add(
                    player.uitools.React.DOM.div({
                            id: 'parallelIndication',
                            zIndex: 3,
                            scaling: false,
                            boxing: false,
                            style: {
                                width: '100%',
                                height: '100%',
                                visibility: 'visible'
                            }
                        },
                        player.gui.InterludeParallelIndication({
                            id: 'parallelIndication',
                            skinSource: 'defaultIndication'
                        })
                    )
                );
                if (JSON.parse(skinsJSON).length > 0) {
                    player.gui.addSkin(JSON.parse(skinsJSON));
                }
                if (Object.keys(skinsJS).length > 0) {
                    // Map the skinJS object into an array of skin objects (where "id"= key)
                    var skinsArray = Object.keys(skinsJS).map(function (key) {
                        skinsJS[key].id = key;
                        return skinsJS[key];
                    });
                    player.gui.addSkin(skinsArray);
                }
                player.gui.add(overlays.overlays);

                head = (typeof head === 'function') ? head() : head;

                player.append(head);

            },

            // the player options for this project
            playerOptions: JSON.parse(playerOptions),

            // Will be called automatically at the start of the headnode.
            onStarted: function () {
                // player.overlays.show();

                player.on('nodestart', onNodeStart, 'demo');
            },

            // Will be called by Helix once the project is about to reach its end.
            // Responsible for cleanup.
            onEnd: function () {
                // do something...
                player.off(null, null, 'demo');
            },

            // The onLoad hook runs at the loading of the html page that loads the project
            // Here you can perform actions that you need to run before the init of the player
            // If you're hook is doing anything asynchronous you need to return a promise. You can use the when.js libaray
            // that is exposed on the ctx object (e.g. return ctx.when.promise())
            onLoad: function (ctx) { // jshint ignore:line

                return ctx.when.promise(function(resolve, reject) {
                    ctx.loadJS(['stylesheets/main.css'], {
                        success: function() {
                            resolve();
                        },
                        error: function() {
                            reject();
                        }
                    });
                });

            }
        };
    });
