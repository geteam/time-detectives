define([
    'resources/ivds'
    ], function(ivds) {
        var nodes = [
            {
                id: 'intro',
                source: ivds.vid_A,
                data: {}
            },
            {
                id: 'firstSuspectChen',
                source: ivds.vid_1stChoice,
                data: {}
            },
            {
                id: 'punchChen',
                source: ivds.vid_2ndChoiceWhiskey,
                data: {}
            },
            {
                id: 'firstSuspectGideon',
                source: ivds.vid_2ndChoiceSake,
                data: {}
            },
            {
                id: 'punchGideon',
                source: ivds.vid_3ndChoiceSake,
                data: {}
            },
            {
                id: 'gideonFinalSpeech',
                source: ivds.vid_3ndChoiceWhiskey,
                data: {}
            },
            {
                id: 'chenFinalSpeech',
                source: ivds.vid_BrunoJoke1MusicASamba,
                data: {}
            },
            {
                id: 'chenGuilty',
                source: ivds.vid_BrunoJoke1MusicBParty,
                data: {}
            },
            {
                id: 'gideonGuilty',
                source: ivds.vid_GameOverFromHiding,
                data: {}
            },
            {
                id:'time_detectives.A',
                source:ivds.vid_A,
                data: {}
            },
            {
                id:'time_detectives.1stChoice',
                source:ivds.vid_1stChoice,
                data: {}
            },
            {
                id:'time_detectives.2ndChoiceSake',
                source:ivds.vid_2ndChoiceSzake,
                data: {}
            },
            {
                id:'time_detectives.2ndChoiceWhiskey',
                source:ivds.vid_2ndChoiceWhiskey,
                data: {}
            },
            {
                id:'time_detectives.3ndChoiceSake',
                source:ivds.vid_3ndChoiceSake,
                data: {}
            },
            {
                id:'time_detectives.3ndChoiceWhiskey',
                source:ivds.vid_3ndChoiceWhiskey,
                data: {}
            },
            {
                id:'time_detectives.BrunoJoke1MusicASamba',
                source:ivds.vid_BrunoJoke1MusicASamba,
                data: {}
            },
            {
                id:'time_detectives.BrunoJoke1MusicBParty',
                source:ivds.vid_BrunoJoke1MusicBParty,
                data: {}
            },
            {
                id:'time_detectives.GameOverTheEnd',
                source:ivds.vid_GameOverTheEnd,
                data: {}
            },
            ];
    return nodes;
    }
);
